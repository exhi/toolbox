import companies from './rest/CompanyRegistry';
import companyTrades from './rest/CompanyTrades';
import salesFigures from './rest/SalesFigures';
import usStates from './rest/USStates';
import {movies} from './rest/Movies';
import {restaurants} from './rest/Restaurants';

export {companies, companyTrades, salesFigures, movies, usStates, restaurants};